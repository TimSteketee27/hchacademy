<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use App\Models\User;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      app()[PermissionRegistrar::class]->forgetCachedPermissions();

      // create permissions
      Permission::create(['name' => 'test permission']);

      // create roles and assign existing permissions
      $role1 = Role::create(['name' => 'writer']);
      $role1->givePermissionTo('test permission');

      $role2 = Role::create(['name' => 'admin']);
      $role2->givePermissionTo('test permission');

      $role3 = Role::create(['name' => 'super-admin']);

      User::where('email', 'timsteketee@hotmail.com')->first()->assignRole($role3);
    }
}
